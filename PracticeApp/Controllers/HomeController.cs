﻿using LinqToExcel;
using PracticeApp.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace PracticeApp.Controllers
{
    public class HomeController : Controller
    {
        private WebAPIDBEntities db = new WebAPIDBEntities();

        public ActionResult Index()
        {
            Session["RoleID"] = null;

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(UserRole userrole)
        {
            if (String.IsNullOrEmpty(userrole.username) || String.IsNullOrEmpty(userrole.RolePassword))
            {
                ViewBag.Msg = "Pls enter both username and password.";
                return View();
            }
            using (var db = new WebAPIDBEntities())
            {
                var obj = db.UserRoles.FirstOrDefault(u => u.username.ToUpper().Equals(userrole.username.ToUpper())
                                                                && u.RolePassword.Equals(userrole.RolePassword));
                if (obj != null && obj.RoleID != 3)
                {
                    Session["RoleID"] = obj.RoleID;
                    return RedirectToAction("PhoneLogList", new { roleID = int.Parse(Session["RoleID"].ToString()) });
                //    return RedirectToAction("PhoneLogList");
                }

                if (obj != null && obj.RoleID == 3)
                {
                    Session["RoleID"] = obj.RoleID;
                    return RedirectToAction("UploadView", new { roleID = int.Parse(Session["RoleID"].ToString()) });
                }

            }
            ViewBag.Msg = "Incorrect username or password. Or you may need to register.";

            return View();
        }



        public ActionResult PhoneLog(int roleID)
        {

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult PhoneLog(int roleID, string userPhone, string otherPhone, string direction, string length, string timestamp, PhoneLog phoneLog)
        {
            if (ModelState.IsValid)
            {
                using (var db = new WebAPIDBEntities())
                {
                    var a = new PhoneLog();
                    a.userPhone = phoneLog.userPhone;
                    a.otherPhone = phoneLog.otherPhone;
                    a.direction = phoneLog.direction;
                    a.length = phoneLog.length;
                    a.timestamp = phoneLog.timestamp;


                    db.PhoneLogs.Add(a);
                    db.SaveChanges();
                    //return RedirectToAction("PhoneLogList");
                    return RedirectToAction("PhoneLogList", new { roleID = int.Parse(Session["RoleID"].ToString()) });


                }
            }

            return View(phoneLog);
        }


        public ActionResult PhoneLogList(int roleID)
        {
            if(roleID == 1)
            {
                TempData["Msg"] = "Welcome CSO";
            }

            if(roleID == 2)
            {
                TempData["Msg"] = "Welcome Data Protection Officer";

            }
            var db = new WebAPIDBEntities();
            List<PhoneLog> vList = new List<PhoneLog>();

            foreach (var rec in db.PhoneLogs)
            {
                vList.Add(new PhoneLog()
                {
                    userPhone = rec.userPhone,
                    otherPhone = rec.otherPhone,
                    direction = rec.direction,
                    length = rec.length,
                    timestamp = rec.timestamp,

                });

            }
            return View(vList);
        }

        public ActionResult DeletePhoneLog(string timestamp)
        {
            var rec = db.PhoneLogs.FirstOrDefault(u => u.timestamp.Equals(timestamp));

            return View(rec);
        }




        [HttpPost, ActionName("DeletePhoneLog")]
        [ValidateAntiForgeryToken]
        public ActionResult DeletePhone(string timestamp)
        {
            var rec = db.PhoneLogs.FirstOrDefault(u => u.timestamp.Equals(timestamp));
            db.PhoneLogs.Remove(rec);
            db.SaveChanges();
            return RedirectToAction("PhoneLogList", new { roleID = int.Parse(Session["RoleID"].ToString()) });

        }





        public ActionResult UploadView(int roleid)
        {
            TempData["Msg"] = "Welcome Data Handler";

            return View();
        }


        [HttpPost]
        public ActionResult UploadView(PhoneLog phoneLog, HttpPostedFileBase FileUpload)
        {

            List<string> data = new List<string>();
            if (FileUpload != null)
            {
                // tdata.ExecuteCommand("truncate table OtherCompanyAssets");  
                if (FileUpload.ContentType == "application/vnd.ms-excel" || FileUpload.ContentType == "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                {


                    string filename = FileUpload.FileName;
                    string targetpath = Server.MapPath("~/Doc/");
                    FileUpload.SaveAs(targetpath + filename);
                    string pathToExcelFile = targetpath + filename;
                    var connectionString = "";
                    if (filename.EndsWith(".xls"))
                    {
                        connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0; data source={0}; Extended Properties=Excel 8.0;", pathToExcelFile);
                    }
                    else if (filename.EndsWith(".xlsx"))
                    {
                        connectionString = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1\";", pathToExcelFile);
                    }

                    var adapter = new OleDbDataAdapter("SELECT * FROM [Sheet1$]", connectionString);
                    var ds = new DataSet();

                    adapter.Fill(ds, "ExcelTable");

                    DataTable dtable = ds.Tables["ExcelTable"];

                    string sheetName = "Sheet1";

                    var excelFile = new ExcelQueryFactory(pathToExcelFile);
                    var artistAlbums = from a in excelFile.Worksheet<PhoneLog>(sheetName) select a;

                    foreach (var a in artistAlbums)
                    {
                        try
                        {
                            if (a.userPhone != "" && a.otherPhone != "" && a.direction != "" && a.length != "" && a.timestamp != "")
                            {
                                PhoneLog TU = new PhoneLog();
                                TU.userPhone = a.userPhone;
                                TU.otherPhone = a.otherPhone;
                                TU.direction = a.direction;
                                TU.length = a.length;
                                TU.timestamp = a.timestamp;


                                db.PhoneLogs.Add(TU);

                                db.SaveChanges();



                            }
                            else
                            {
                                data.Add("<ul>");
                                if (a.userPhone == "" || a.userPhone == null) data.Add("<li> userPhone is required</li>");
                                if (a.otherPhone == "" || a.otherPhone == null) data.Add("<li> otherPhone is required</li>");
                                if (a.direction == "" || a.direction == null) data.Add("<li> direction is required</li>");
                                if (a.length == "" || a.length == null) data.Add("<li> length is required</li>");
                                if (a.timestamp == "" || a.timestamp == null) data.Add("<li> timestamp is required</li>");

                                data.Add("</ul>");
                                data.ToArray();
                                return Json(data, JsonRequestBehavior.AllowGet);
                            }
                        }

                        catch (DbEntityValidationException ex)
                        {
                            foreach (var entityValidationErrors in ex.EntityValidationErrors)
                            {

                                foreach (var validationError in entityValidationErrors.ValidationErrors)
                                {

                                    Response.Write("Property: " + validationError.PropertyName + " Error: " + validationError.ErrorMessage);

                                }

                            }
                        }
                    }
                    //deleting excel file from folder  
                    if ((System.IO.File.Exists(pathToExcelFile)))
                    {
                        System.IO.File.Delete(pathToExcelFile);
                    }
                    //  return Json("success", JsonRequestBehavior.AllowGet);

                    return View(Session["RoleID"]);

                }
                else
                {
                    //alert message for invalid file format  
                    data.Add("<ul>");
                    data.Add("<li>Only Excel file format is allowed</li>");
                    data.Add("</ul>");
                    data.ToArray();
                    return Json(data, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                data.Add("<ul>");
                if (FileUpload == null) data.Add("<li>Please choose Excel file</li>");
                data.Add("</ul>");
                data.ToArray();
                return Json(data, JsonRequestBehavior.AllowGet);
            }
        }


        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Customer()
        {
            var db = new WebAPIDBEntities();
            List<Customer> vList = new List<Customer>();

            foreach (var rec in db.Customers)
            {
                vList.Add(new Customer()
                {
                    FirstName = rec.FirstName,
                    EmailAddress = rec.EmailAddress,
                    LastName = rec.LastName,
                    PhoneNo = rec.PhoneNo,
                    PolicyNo = rec.PolicyNo,
                    Address = rec.Address
                });

            }
            // ViewBag.Msg = "Your Application was successful!";
            //    ViewBag.isTrue = isTrue;

            return View(vList);
        }

        public ActionResult Create()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CustomerID,LastName,FirstName,EmailAddress,PolicyNo,Address,PhoneNo,DateOfBirth")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Customers.Add(customer);
                db.SaveChanges();
                return RedirectToAction("Customer");
            }

            return View(customer);
        }

        public ActionResult Edit(string policyNo)
        {
            var rec = db.Customers.FirstOrDefault(u => u.PolicyNo.Equals(policyNo));
            if (rec != null)
            {
                return View(rec);

            }

            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CustomerID,LastName,FirstName,EmailAddress,PolicyNo,Address,PhoneNo,DateOfBirth")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Customer");
            }
            return View(customer);
        }



        public ActionResult Details(string policyNo)
        {
            var rec = db.Customers.FirstOrDefault(u => u.PolicyNo.Equals(policyNo));
            if (rec != null)
            {
                return View(rec);

            }

            return View();
        }


        public ActionResult Delete(string policyNo)
        {
            var rec = db.Customers.FirstOrDefault(u => u.PolicyNo.Equals(policyNo));

            return View(rec);
        }




        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string policyNo)
        {
            var rec = db.Customers.FirstOrDefault(u => u.PolicyNo.Equals(policyNo));
            db.Customers.Remove(rec);
            db.SaveChanges();
            return RedirectToAction("Customer");

        }
          protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }



    }
}